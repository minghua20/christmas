import React from "react";
import { useNavigate } from "react-router-dom";
import "./Gift.scss";
import { GiGoose } from "react-icons/gi";


const Gift = () => {

    const navigate = useNavigate();

    return (
      <div className="main clearfix">
        <div className="santa">
          <div className="words">
            <p>To VanHui:</p>
          </div>
          <div className="cap"></div>
          <div className="face">
            <div className="eyes">
              <div className="left">
                <span></span>
              </div>
              <div className="right">
                <span></span>
              </div>
            </div>
            <div className="mouth">
              <div className="nose"></div>
            </div>
          </div>
          <div className="body">
            <div className="left-hand"></div>
            <div className="right-hand"></div>
            <div className="belt">
              <div className="buckle"></div>
            </div>
            <div className="legs">
            </div>
          </div>
        </div>
        <div className="gift-holder" onClick={() => navigate("/merrychristmas")}>
          <div className="gift">
            <div className="ribbon"></div>
            <div className="goose1">
                <GiGoose />
            </div>
          </div>
          <div className="gift purple">
            <div className="ribbon"></div>
            <div className="goose2">
                <GiGoose />
            </div>
          </div>
          <div className="gift orange">
            <div className="ribbon"></div>
            <div className="goose3">
                <GiGoose />
            </div>
          </div>
        </div>
      </div>
    )
}

export default Gift;
