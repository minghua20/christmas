import { HashRouter, Route, Routes } from 'react-router-dom';
import Gift from './Gift';
import MerryChristmas from './MerryChristmas';

function App() {

  return (
    // <BrowserRouter>
    <HashRouter>
      <Routes path="/">
        <Route index element={<Gift />} />
        <Route path="/merrychristmas" element={<MerryChristmas />} />
      </Routes>
    {/* </BrowserRouter> */}
    </HashRouter>
  );
}

export default App;
